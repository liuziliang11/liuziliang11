---
layout: category
title: 网页平面设计
date: 2023-01-02
excerpt_separator: "<!--more-->"
categories:
     - 平面设计
---

1、网页的基本元素：文字、图像、超链接  


2、HTML的基本机构head、title、body三部分 
![](/assets/images/Example-diagram-10.png) 
 <!--more--> 

网页平面设计，要明白网页的基本构造，我们常见的网页有什么形式，例如企业的官网，新闻类的信息流网站，购物的网站，他们的样式等都是不同的，设计时也不会相同。  
# 如企业官网注重的是突出企业内容，无太多分类导航栏  
![](/assets/images/Example-diagram-7.png)  
# 新闻类网站查询功能要完善，导航栏与分类较多，并且还有许多大字报来显出新闻热点。  
![](/assets/images/Example-diagram-8.png)  
# 购物类网站功能区块更多，把更多内容的选择交给用户决定  
![](/assets/images/Example-diagram-9.png)  
**综上，网页的平面设计要根据不同网页功能来制定，对不同功能要有不同的个性化和功能化**   
